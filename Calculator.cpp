#include <iostream>
#include <cstdlib>
#include <vector>
#include <unordered_map>

using namespace std;

class Lexer{
  private:
    class Token{
      public:
        string tokenType;
        string tokenValue;
        Token(string tt, string tv){
          tokenType = tt;
          tokenValue = tv;
        }
    };

    vector<Token> listOfTokens;

  public:

};
