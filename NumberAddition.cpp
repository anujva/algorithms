#include <iostream>
#include <sstream>

using namespace std;

int NumberAddition(string str) { 

  // code goes here
  bool firstNumberFound = false;
  int firstIndex = 0;
  int length = 0;
  int sum = 0;
  
  for(int i=0; i<(int)str.size(); i++){
    if(str[i] > 47 && str[i] < 58){
      if(!firstNumberFound){
        firstNumberFound = true;
        firstIndex = i;
        length = 0;
      }else{
        if(firstNumberFound)
          length ++;
      }
    }else{
      if(firstNumberFound){
        stringstream oss(str.substr(firstIndex, length+1));
        int temp; oss >> temp;
        sum += temp;
        firstNumberFound = false;
      }
    }
  }
  if(firstNumberFound){
    stringstream oss(str.substr(firstIndex, length+1));
    int temp; oss >> temp;
    sum += temp;
    firstNumberFound = false;
  }
  return sum;          
}

int main() { 
  
  // keep this function call here
  string str = "10 2One Number*1*";
  cout << NumberAddition(str) << endl;
  return 0;
} 
