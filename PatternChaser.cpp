#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <sstream>
#include <algorithm>

using namespace std;

bool compare(string str1, string str2){
  if(str1.size() > str2.size()){
    return true;
  }
  return false;
}

string PatternChaser(string str){
  vector<string> patternMatches;
  for(int i=2; i < (int)str.size()/2; i++){
    for(int j=0; j < (int)str.size()-i; j++){
      //for each pattern of length i, search for it through the entire length
      string substring = str.substr(j, i);
      int count = -1;
      for(int k=0; k < (int)str.size()-i+1; k++){
        int different = false;
        for(int l=0; l < (int)substring.size(); l++){
          if(str[k+l] == substring[l]){
            continue;
          }else{
            different = true;
            break;
          }
        }
        if(!different){
          count++;
        }
      }
      if(count > 0){
        patternMatches.push_back(substring);
      }
    }
  }
  sort(patternMatches.begin(), patternMatches.end(), compare);
  if(patternMatches.size() > 0){
    return string("yes ") + patternMatches[0];
  }
  return string("no null");
}

int main(){
  string str = string("aabejiabkfabed");
  cout << PatternChaser(str) << endl;
  return 0;
}
