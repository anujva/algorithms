#include <iostream>
#include <vector>
#include <string>

using namespace std;

class TelegramProblem{
  private:
    int w;
    int MAXLENGTHOFDOC;
    vector< vector<char> > lines;
    string currentWord;
    int currentLine;
    int spacesLeft;

  public:
    TelegramProblem(int W) : MAXLENGTHOFDOC(30){
      w = W;
      currentLine = 0;
      spacesLeft = w;
      lines.resize(MAXLENGTHOFDOC);
    }

    //w is the max number of characters possible in a line.
    //words cannot be split into two different lines.
    void getWordsInLine(char a){
      currentWord += a;
      if(a == ' '){
        //test if the current word will fit the line.
        if((int)currentWord.size() - 1 <= spacesLeft){
          for(int i=0; i < (int)currentWord.size(); i++){
            lines[currentLine].push_back(currentWord[i]);
            spacesLeft--;
          }
          currentWord = "";
        }else{
          currentLine++;
          spacesLeft = w;
          for(int i=0; i < (int)currentWord.size(); i++){
            lines[currentLine].push_back(currentWord[i]);
          }
          currentWord = "";
        }
      }
    }

    void printLines(){
      for(int i=0; i <= currentLine; i++){
        for(int j=0; j < w; j++){
          cout << lines[i][j];
        }
        cout << endl;
      }
    }
};

int main(){
  TelegramProblem tp(80);
  string lines;
  getline(cin, lines);
  for(int i=0; i < (int)lines.length(); i++){
    tp.getWordsInLine(lines[i]);
  }
  tp.printLines();
}
