#include <iostream>
#include <cstdlib>
#include <vector>
#include <sstream>

using namespace std;

string Calculator(string str) { 
  // code goes here   
  vector<int> numbers;
  vector<char> operators;
  int startIndex = 0;
  int endIndex = 0;
  for(int i=0; i<str.size(); i++){
    if(str[i] > 47 && str[i] < 58){
      endIndex ++;
    }else{
      if(str[i] == '('){
        startIndex = i+1;
        endIndex = 0;
        continue;
      }
      if(str[i] == ')'){
        //do something with the stacks
        if(endIndex > 0){
          stringstream oss(str.substr(startIndex, endIndex));
          int value;
          oss >> value;
          numbers.push_back(value);
        }
        int val1 = numbers[numbers.size()-1]; numbers.pop_back();
        int val2 = numbers[numbers.size()-1]; numbers.pop_back();
        int result = 0;
        if(operators[operators.size()-1] == '*'){
          result = val1 * val2;
        }else if(operators[operators.size()-1] == '+'){
          result = val1 + val2;
        }else if(operators[operators.size()-1] == '-'){
          result = val2 - val1;
        }else if(operators[operators.size()-1] == '/'){
          result = val2 / val1;
        }
        numbers.push_back(result);
        operators.pop_back();
        startIndex = i+1;
        endIndex = 0;
      }else{
        operators.push_back(str[i]);
        stringstream oss;
        if(endIndex > 0){
          oss << str.substr(startIndex, endIndex);
          int value;
          oss >> value;
          numbers.push_back(value);
        }
        startIndex = i+1;
        endIndex = 0;
      }
    }
  }

  if(endIndex > 0){
    stringstream oss(str.substr(startIndex, endIndex));
    int valval;
    oss >> valval;
    numbers.push_back(valval);
  }

  while(operators.size()!=0){
    int val1 = numbers[numbers.size()-1]; numbers.pop_back();
    int val2 = numbers[numbers.size()-1]; numbers.pop_back();
    int result = 0;
    if(operators[operators.size()-1] == '*'){
      result = val1 * val2;
    }else if(operators[operators.size()-1] == '+'){
      result = val1 + val2;
    }else if(operators[operators.size()-1] == '-'){
      result = val2 - val1;
    }else if(operators[operators.size()-1] == '/'){
      result = val2 / val1;
    }
    numbers.push_back(result);
    operators.pop_back();
  }
  stringstream oss;
  oss << numbers[0];
  return oss.str(); 
}

int main() { 
  // keep this function call here
  cout << Calculator("((6*(4/2))+3*1)") << endl;
  return 0;
}
