#include <iostream>
#include <sstream>
#include <cstring>
#include <string>
#include <cmath>
#include <vector>
#include <set>

using namespace std;

int ThreeFiveMultiples(int num){
  int sum = 0;
  set<int> allMultiples;
  int num1 = 3;
  int num2 = 5;
  while(num1 < num){
    allMultiples.insert(num1);
    num1 += 3;
  }
  while(num2 < num){
    allMultiples.insert(num2);
    num2 += 5;
  }
  set<int>::iterator itr;
  for(itr = allMultiples.begin(); itr != allMultiples.end(); itr++){
    sum += *itr;
  }

  return sum;
}

int main(){
  int num = 10;
  cout << ThreeFiveMultiples(num) << endl;
  return 0;
}
