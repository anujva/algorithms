#include <iostream>
#include <vector>
#include <sstream>

using namespace std;

vector<string> permute(string number){
  vector<string> listAll;
  if(number.size() == 1){
    listAll.push_back(number);
    return listAll;
  }
  for(int i=0; i < (int)number.size(); i++){
    char temp = number[i];
    string number_placeholder = number;
    number.erase(number.begin()+i);
    vector<string> stringVec = permute(number);
    number = number_placeholder;
    for(int j=0; j < (int)stringVec.size(); j++){
      string store1 = stringVec[j] + temp; 
      string store2 = temp + stringVec[j];
      bool testPresence = false;
      for(int k=0; k < (int)listAll.size(); k++){
        if(listAll[k].compare(store1) == 0){
          testPresence = true;
          break;
        }
      }
      if(!testPresence)
        listAll.push_back(store1);
      testPresence = false;
      for(int k=0; k < (int)listAll.size(); k++){
        if(listAll[k].compare(store1) == 0){
          testPresence = true;
          break;
        }
      }
      if(!testPresence)
        listAll.push_back(store2);
    }
  }
  return listAll;
}

bool testPrime(string number){
  stringstream oss(number);
  int num;
  oss >> num;
  if(num == 2 || num == 3 || num == 5 || num == 7 || num == 11){
    return true;
  }

  if((num % 2 == 0) || (num % 3 == 0) || (num % 5 == 0) || (num % 7) == 0){
    return false;
  }

  for(int i=11; i < num; i = i+2){
    if(num % i == 0){
      return false;
    }
  }

  return true;
}

int PrimeChecker(int num){
  stringstream oss;
  oss << num;
  string number = oss.str();
  vector<string> permutations;
  permutations = permute(number);
  for(int i=0; i < (int)permutations.size(); i++){
    if(testPrime(permutations[i])){
      return 1;
    }
  }
  return 0;
}

int main(){
  int num = 990;
  cout << PrimeChecker(num) << endl;
  return 0;
}
