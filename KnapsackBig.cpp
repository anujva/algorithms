#include <iostream>
#include <ctime>
#include <vector>
#include <fstream>

using namespace std;

class KnapSackItems{
  public:
    long long weight;
    long long value;

    KnapSackItems(long long w, long long v);
};

KnapSackItems::KnapSackItems(long long w, long long v){
  weight = w;
  value = v;
}

class KnapSack{
  private:
    vector< vector<long long> > sack;
    long long numberOfItems;
    long long maxCapacity;
    vector<KnapSackItems> items;

  public:
    KnapSack(fstream &myfile);
    long long solution;
    long long max(long long a, long long b);
};

KnapSack::KnapSack(fstream &myfile){
  myfile >> maxCapacity >> numberOfItems;
  while(myfile.good()){
    long long w=0, v=0;
    myfile >> v >> w;
    items.push_back(KnapSackItems(w, v));
  }
  sack.resize(2);
  for(long long i=0; i<=1; i++){
    sack[i].resize(maxCapacity+1);
  }
  
  for(long long i=0; i<=maxCapacity; i++){
    sack[0][i] = 0;
  }
  
  clock_t t = clock();
  for(long long i=1; i<=numberOfItems; i++){
    for(long long j=0; j<=maxCapacity; j++){
      if(j - items[i-1].weight > 0){
        sack[1][j] = max(sack[0][j], sack[0][j-items[i-1].weight] + items[i-1].value);       
      }else{
        //cout<<i<<" "<<j<<endl;
        sack[1][j] = sack[0][j];
      }
    }
    for(long long k=0; k<=maxCapacity; k++){
      sack[0][k] = sack[1][k];
      //copying all the 1's to 0's and its ready for the new i;
    }
  }

  t=clock()-t;
  cout<<"The time taken to compute the value of this knapsack: "<<t<<" milliseconds or "<<(float)t/CLOCKS_PER_SEC<<" seconds"<<endl;
  solution = sack[1][maxCapacity];
}

long long KnapSack::max(long long a, long long b){
  if(a>b){
    return a;
  }else{
    return b;
  }
}


int main(int argc, char *argv[]){
  fstream myfile;
  myfile.open("knapsack_big.txt", ios::in);
  KnapSack knapsack(myfile);
  cout<<"The value of optimal solution is: "<<knapsack.solution<<endl;
}
