#include <iostream>
#include <cstring>
#include <string>
#include <vector>

using namespace std;

int BracketMatcher(string str){
  vector<char> stack;
  for(int i=0; i < (int)str.size(); i++){
    if(str[i] == '('){
      stack.push_back('(');
    }
    if(str[i] == ')'){
      stack.pop_back();
    }
  }

  if(stack.size() == 0){
    return 1;
  }

  return 0;
}

int main(){
  string str = "(c(oder)) b(yte)";
  cout << BracketMatcher(str) << endl;
  return 0;
}
