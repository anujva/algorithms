#include <iostream>
#include <vector>
#include <cstring>
#include <string>
#include <algorithm>
#include <sstream>

using namespace std;

int PermutationStep(int num){
  stringstream oss;
  oss << num;
  string number = oss.str();
  for(int i=number.size()-2; i>-1; i--){
    string temp = number.substr(i+1, number.size() - i - 1);
    sort(temp.begin(), temp.end());
    if(number[i] > temp[temp.size() - 1]){
      continue;
    }else{
      //swap it with the number that is the next highest to the current number;
      for(int j=0; j<(int)temp.size(); j++){
        if(number[i] >= temp[j]){
          continue;
        }else{
          char temp1 = temp[j];
          temp[j] = number[i];
          number[i] = temp1;
          number.erase(i+1, number.size() - i - 1);
          number += temp;
          stringstream oss1(number);
          int returnthis;
          oss1 >> returnthis;
          return returnthis;
        }
      }
    }
  }
  return -1;
}

int main(){
  cout << PermutationStep(897654321) << endl;
}
