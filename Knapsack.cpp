#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

class KnapSackItems{
  public:
    long long weight;
    long long value;

    KnapSackItems(long long w, long long v);
};

KnapSackItems::KnapSackItems(long long w, long long v){
  weight = w;
  value = v;
}

class KnapSack{
  private:
    vector< vector<long long> > sack;
    long long numberOfItems;
    long long maxCapacity;
    vector<KnapSackItems> items;

  public:
    KnapSack(fstream &myfile);
    long long solution;
    long long max(long long a, long long b);
};

KnapSack::KnapSack(fstream &myfile){
  myfile >> maxCapacity >> numberOfItems;
  while(myfile.good()){
    long long w=0, v=0;
    myfile >> v >> w;
    items.push_back(KnapSackItems(w, v));
  }
  sack.resize(numberOfItems+1);
  for(long long i=0; i<=numberOfItems; i++){
    sack[i].resize(maxCapacity+1);
  }
  
  for(long long i=0; i<=maxCapacity; i++){
    sack[0][i] = 0;
  }

  for(long long i=1; i<=numberOfItems; i++){
    for(long long j=0; j<=maxCapacity; j++){
      if(j - items[i-1].weight > 0){
        sack[i][j] = max(sack[i-1][j], sack[i-1][j-items[i-1].weight] + items[i-1].value);       
      }else{
        cout<<i<<" "<<j<<endl;
        sack[i][j] = sack[i-1][j];
      }
    }
  }

  solution = sack[numberOfItems][maxCapacity];
}

long long KnapSack::max(long long a, long long b){
  if(a>b){
    return a;
  }else{
    return b;
  }
}


int main(int argc, char *argv[]){
  fstream myfile;
  myfile.open("knapsack1.txt", ios::in);
  KnapSack knapsack(myfile);
  cout<<"The value of optimal solution is: "<<knapsack.solution<<endl;
}
