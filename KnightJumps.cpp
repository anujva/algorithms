#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <utility>

using namespace std;

pair<int, int> parseString(string str){
  stringstream oss1, oss2;
  oss1 << str[1];
  oss2 << str[3];
  int x, y;
  oss1 >> x;
  oss2 >> y;
  return pair<int, int>(x-1, y-1);
}

vector< pair<int, int> > getValidMoves(pair<int, int> position){
  //knight moves 2 positions in one direction and 1 position in the other direction
  vector<pair<int, int> > results;
  results.push_back(pair<int, int>(position.first + 2, position.second + 1));
  results.push_back(pair<int, int>(position.first - 2, position.second + 1));
  results.push_back(pair<int, int>(position.first + 2, position.second - 1));
  results.push_back(pair<int, int>(position.first - 2, position.second - 1));
  results.push_back(pair<int, int>(position.first + 1, position.second + 2));
  results.push_back(pair<int, int>(position.first + 1, position.second - 2));
  results.push_back(pair<int, int>(position.first - 1, position.second + 2));
  results.push_back(pair<int, int>(position.first - 1, position.second - 2));
  
  vector< pair<int, int> > finalresults;

  for(vector< pair<int, int> >::iterator it = results.begin(); it != results.end(); it++){
    if(!(it->first < 0 || it->second < 0 || it->first > 7 || it->second > 7)){
      finalresults.push_back(*it);
    }
  }

  return finalresults;
}

string KnightJumps(string str){
  pair<int, int> positionOfKnight = parseString(str);
  vector<pair<int, int> > validPositions = getValidMoves(positionOfKnight);
  stringstream oss;
  oss << validPositions.size();
  return oss.str();
}

int main(){
  string str("(2 8)");
  cout << KnightJumps(str) << endl;
}
