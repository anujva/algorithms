/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package optimalassignments;

import java.util.Scanner;

/**
 *
 * @author securonix
 */
public class OptimalAssignments {
    
    private class TestObject{
        int value;
        int machine_index;
        int job_index;
    }
    
   private class ReturnObject{
       int val;
       String s;
       
       public ReturnObject(int val, String s){
           this.val = val;
           this.s = s;
       }
   }

    public String OptimalAssignmentsFunction(String[] strArr) {
        //we have the arguments here.. 
        TestObject[][] machinevstime = new TestObject[strArr.length][strArr.length];

        for (int i = 0; i < strArr.length; i++) {
            String s = strArr[i].substring(1, strArr[i].length() - 1);
            String[] times = s.split(",");
            assert times.length == strArr.length;
            for (int j = 0; j < strArr.length; j++) {
                machinevstime[i][j] = new TestObject();
                machinevstime[i][j].value = Integer.parseInt(times[j]);
                machinevstime[i][j].machine_index = i+1;
                machinevstime[i][j].job_index = j+1;
            }
        }
        
        //now we basically have to permute the machines in all the positions that it can 
        //work in and we will be able to get the value from it. 
        //we need to find a way to permute the jobs in the different positions
        int minValue = Integer.MAX_VALUE;
        String s = "";
        for (int i = 0; i < machinevstime.length; i++) {
            //size == current length -1;
            TestObject[][] tempMatrix = new TestObject[machinevstime.length - 1][machinevstime.length - 1];
            
            for (int j = 0; j < machinevstime.length - 1; j++) {
                //put in the matrix here.. 
                int col = 0;
                for (int k = 0; k < machinevstime.length; k++) {
                    if (k == i) {
                        continue;
                    }
                    tempMatrix[j][col++] = machinevstime[j][k];
                }
            }
            ReturnObject ro = minimum(tempMatrix);
            if (minValue > machinevstime[machinevstime.length - 1][i].value + ro.val) {
                minValue = machinevstime[machinevstime.length - 1][i].value + ro.val;
                s = ro.s + "(" + machinevstime[machinevstime.length - 1][i].machine_index + "-" + 
                        machinevstime[machinevstime.length-1][i].job_index + ")";
            }
        }
        return s;
    }

    private ReturnObject minimum(TestObject[][] tempMatrix) {
        if (tempMatrix.length == 2) {
            int val = ((tempMatrix[0][0].value + tempMatrix[1][1].value) < (tempMatrix[0][1].value + tempMatrix[1][0].value) ? (tempMatrix[0][0].value + tempMatrix[1][1].value) : (tempMatrix[0][1].value + tempMatrix[1][0].value));
            String s = ((tempMatrix[0][0].value + tempMatrix[1][1].value) < (tempMatrix[0][1].value + tempMatrix[1][0].value) ? ("(" + tempMatrix[0][0].machine_index + "-" 
                    +tempMatrix[0][0].job_index + ")" +
                    "(" + tempMatrix[1][1].machine_index + "-" 
                    + tempMatrix[1][1].job_index + ")") : ("(" +
                    tempMatrix[0][1].machine_index + "-" +tempMatrix[0][1].job_index + ")" +
                    "(" + tempMatrix[1][0].machine_index + "-" + tempMatrix[1][0].job_index + ")"));
            
            return new ReturnObject(val, s);
        }
        
        ReturnObject minValue = new ReturnObject(Integer.MAX_VALUE, "");
        String s = "";
        for (int i = 0; i < tempMatrix.length; i++) {
            TestObject[][] tempMatrix1 = new TestObject[tempMatrix.length - 1][tempMatrix.length - 1];
            for (int j = 0; j < tempMatrix.length-1; j++) {
                int col = 0;
                for (int k = 0; k < tempMatrix.length; k++) {
                    if (k == i) {
                        continue;
                    }
                    tempMatrix1[j][col++] = tempMatrix[j][k];
                }
            }
            ReturnObject ro = minimum(tempMatrix1);
            if (minValue.val > tempMatrix[tempMatrix.length - 1][i].value + ro.val) {
                minValue.val = tempMatrix[tempMatrix.length - 1][i].value + ro.val;
                minValue.s = ro.s + "(" + tempMatrix[tempMatrix.length - 1][i].machine_index + "-" +
                        tempMatrix[tempMatrix.length - 1][i].job_index + ")";
            }
        }
        
        return minValue;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner s = new Scanner(System.in);
        OptimalAssignments oa = new OptimalAssignments();
        String line;
        while ((line = s.nextLine()) != null) {
            if (line.length() == 0) {
                break;
            }
            line = line.substring(1, line.length() - 1);
            String[] splitString = line.split("\",\"");
            String str = oa.OptimalAssignmentsFunction(splitString);
            System.out.println("The min permutation comes out to be: " + str);
        }
    }

}
