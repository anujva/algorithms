#include <iostream>

using namespace std;

int GCD(int num1, int num2){
  if(num1 == num2){
    return num1;
  }else if(num1 > num2){
    return GCD(num2, num1-num2);
  }else if(num2 > num1){
    return GCD(num1, num2-num1);
  }
}

int main(){
  cout << GCD(100, 100) << endl;
  return 0;
}
