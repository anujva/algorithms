#include <iostream>

using namespace std;

void cubes(int n){
  if( n > 0){
    cout << n*n*n << endl;
    cubes(n-1);
  }else{
    return;
  }
}

int main(){
  cubes(10);
}
