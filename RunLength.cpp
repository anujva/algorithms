#include <iostream>
#include <sstream>
#include <cstring>
#include <vector>

using namespace std;

string RunLength(string str){
  string runLengthString = string("");
  int count = 1;
  for(int i=0; i < (int)str.size()-1; i++){
    if(str[i] == str[i+1]){
      count++;
      continue;
    }else{
      stringstream oss;
      oss << count;
      runLengthString += oss.str() + str[i];
      count = 1;
    }
  }
  if(str[str.size()-1] != str[str.size()-2]){
    stringstream oss;
    oss << 1;
    runLengthString += oss.str() + str[str.size() - 1];
  }else{
    stringstream oss;
    oss << count;
    runLengthString += oss.str() + str[str.size() - 1];
  }
  return runLengthString;
}

int main(){
  string str = "wwwbbbww";
  cout << RunLength(str) << endl;
  return 0;
}
