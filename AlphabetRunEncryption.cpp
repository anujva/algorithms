#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <utility>

using namespace std;

string AlphabetRunEncryption(string str){
  stringstream oss;

  char oldChar = str[0];
  int firstIndex = 0;
  vector< pair<char, char> > decryptedDuets;
  bool forward = true;

  for(int i=1; i < (int)str.size(); i++){
    if(str[i] == oldChar+1){
      oldChar = str[i];
      continue;
    }else if(str[i] == oldChar-1){
      oldChar = str[i];
      continue;
    }else if(str[i] == 'N'){
      decryptedDuets.push_back(make_pair(str[firstIndex]-1, str[i-1]));
      decryptedDuets.push_back(make_pair(str[i-1], str[i-1]));
      if(i+1 < (int)str.size()){
        oldChar = str[i+1];
        firstIndex = i+1;
        i++;
      }else{
        forward = false;
      }
    }else if(str[i] == 'R'){
      decryptedDuets.push_back(make_pair(str[i-1]-1, str[i-1]+1));
      if(i+1 < (int)str.size()){
        oldChar = str[i+1];
        firstIndex = i+1;
        i++;
      }else{
        forward = false;
      }
    }else if(str[i] == 'L'){
      decryptedDuets.push_back(make_pair(str[i-1]+1, str[i-1]-1));
      if(i+1 < (int)str.size()){
        oldChar = str[i+1];
        firstIndex = i+1;
        i++;
      }else{
        forward = false;
      }
    }else if(str[i] == 'S'){
      decryptedDuets.push_back(make_pair(str[i-2], str[i-1]));
      if(i+1 < (int)str.size()){
        oldChar = str[i+1];
        firstIndex = i+1;
        i++;
      }else{
        forward = false;
      }
    }else{
      if(str[firstIndex] - str[i] < 0){
        decryptedDuets.push_back(make_pair(str[firstIndex]-1, str[i]+1));
      }else{
        decryptedDuets.push_back(make_pair(str[firstIndex]+1, str[i]-1));
      }
      oldChar = str[i];
      firstIndex = i;
    }
  }

  if(forward){
    if(str[firstIndex] - str[str.size()-1] < 0){
      decryptedDuets.push_back(make_pair(str[firstIndex]-1, str[str.size()-1]+1));
    }else{
      decryptedDuets.push_back(make_pair(str[firstIndex]+1, str[str.size()-1]-1));
    }
  }
  
  oss << decryptedDuets[0].first;
  for(int i=1; i < (int)decryptedDuets.size(); i++){
    if(decryptedDuets[i].first == decryptedDuets[i-1].second){
      oss << decryptedDuets[i].first;
    }
  }
  oss << decryptedDuets[decryptedDuets.size()-1].second;

  return oss.str();
}

int main(){
  string str = string("abSbaSaNbR");
  cout << AlphabetRunEncryption(str);
  return 0;
}
