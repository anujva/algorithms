#include <iostream>
using namespace std;

string StringScramble(string str1, string str2) { 
  // code goes here
  for(int i=0; i<(int)str2.size(); i++){
    for(int j=0; j<(int)str1.size(); j++){
      if(str2[i] == str1[j]){
        str2.erase(str2.begin()+i);
        str1.erase(str1.begin()+j);
        i--;
        break;
      }
    }
  }
  
  if(str2.size() == 0)
    return "true";
  
  cout << str2.size() << endl;
  return "false";
}

int main() { 
  
  // keep this function call here
  string str1 = "rkqodlw";
  string str2 = "world";
  cout << StringScramble(str1, str2);
  return 0;
    
} 

