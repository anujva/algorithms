#include <iostream>
#include <cstring>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

unsigned int StringReduction(string str){
  for(int i=0; i<(int)str.size()-1; i++){
    if(str[i] == 'a' && str[i+1] == 'b'){
      str.erase(str.begin()+i);
      str.replace(i, 1, "c");
      i = -1;
    }else if(str[i] == 'a' && str[i+1] == 'c'){
      str.erase(str.begin()+i);
      str.replace(i, 1, "b");
      i = -1;
    }else if(str[i] == 'b' && str[i+1] == 'a'){
      str.erase(str.begin()+i);
      str.replace(i, 1, "c");
      i = -1;
    }else if(str[i] == 'b' && str[i+1] == 'c'){
      str.erase(str.begin()+i);
      str.replace(i, 1, "a");
      i = -1;
    }else if(str[i] == 'c' && str[i+1] == 'a'){
      str.erase(str.begin()+i);
      str.replace(i, 1, "b");
      i = -1;
    }else if(str[i] == 'c' && str[i+1] == 'b'){
      str.erase(str.begin()+i);
      str.replace(i, 1, "a");
      i = -1;
    }
  }
  return str.size();
}

int main(){
  string str = "abb";
  cout << StringReduction(str) << endl;
  return 0;
}
