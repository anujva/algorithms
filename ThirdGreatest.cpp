#include <iostream>
#include <vector>
#include <cstring>
#include <algorithm>

using namespace std;

class StringClass{
  public:
    int size;
    string word;

    StringClass(int s, string str){
      size = s;
      word = str;
    }
};

bool comparison(StringClass s1, StringClass s2){
  if(s1.size > s2.size){
    return true;
  }else{
    return false;
  }
}

string ThirdGreatest(vector<string> strArr){
  vector<StringClass> strings; 
  for(int i=0; i < (int)strArr.size(); i++){
    strings.push_back(StringClass(strArr[i].size(), strArr[i]));
  }
  for(int i=0; i < (int)strings.size(); i++){
    cout << strings[i].word << " " << strings[i].size << endl;
  }
  sort(strings.begin(), strings.end(), comparison);
  return strings[2].word;
}

int main(){
  string B[] = {"coder", "byte", "code"};
  vector<string> strArr(B, B+sizeof(B)/sizeof(B[0]));
  cout << ThirdGreatest(strArr) << endl;
  return 0;
}
