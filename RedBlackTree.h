#include <iostream>

template <class T>
class RedBlack{
  private:
    template <class T>
    class RedBlackNode{
      public:
        T data;
        int key;
        bool color;
        void *comparisonFunction;
        RedBlackNode *left;
        RedBlackNode *right;
        RedBlackNode *parent;
    };

    RedBlackNode *root;
    int numberOfNodes;
    int rotateLeft(RedBlackNode &node);
    int rotateRight(RedBlackNode &node);
    int invertColor(RedBlackNode &node);

  public:
    RedBlack();
    RedBlack(int N);

    void insertIntoTree(T data);

    T deleteNode(int key);
};
