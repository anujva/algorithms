#include <iostream>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include <random>

using namespace std;

int choosePivot(int firstIndex, int secondIndex){
  std::default_random_engine generator;
  std::uniform_int_distribution<int> distributor(firstIndex, secondIndex);
  int index = distributor(generator);
  return index;
}

void swap(int &a, int &b){
  int temp = a;
  a = b;
  b = temp;
}

int partitionAroundPivot(vector<int> &array, int pivotIndex, int firstIndex, int secondIndex){
  swap(array[firstIndex], array[pivotIndex]);
  int j=firstIndex;
  for(int i=firstIndex+1; i<=secondIndex; i++){
    if(array[i] < array[firstIndex]){
      swap(array[i], array[j+1]);
      j++;
    }
  }
  swap(array[firstIndex], array[j]);
  return j;
}

void quicksort(vector<int> array, int firstIndex, int secondIndex){
  int pivotIndex = choosePivot(firstIndex, secondIndex);
  int pivot = partitionAroundPivot(array, pivotIndex, firstIndex, secondIndex);
  if(pivot == N){
    return array[pivot];
  }else if(N < pivot){
    quicksort(array, firstIndex, pivot-1);
  }else if(N > pivot){
    quicksort(array, pivot+1, secondIndex);
  }
}

int main(){

}
