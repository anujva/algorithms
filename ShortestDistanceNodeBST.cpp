#include <iostream>
#include <vector>
#include <string>

using namespace std;

class BST{
  private:
    class BSTNode{
      public:
        int data;
        BSTNode *left;
        BSTNode *right;
        BSTNode(){
          left = NULL;
          right = NULL;
        }
    };

    BSTNode *root;

  public:
    BST(){
      root = NULL;
    }

    int addNode(int data){
      if(root == NULL){
        root = new BSTNode();
        root->data = data;
        root->left = NULL;
        root->right = NULL;
        return 0;
      }else{
        BSTNode *temp = root;
        bool dataPushed = false;
        while(!dataPushed && temp != NULL){
          if(temp->data > data){
            if(temp->left == NULL){
              temp->left = new BSTNode();
              temp->left->data = data;
              dataPushed = true;
            }else{
              temp = temp->left;
            }
          }else if(temp->data < data){
            if(temp->right == NULL){
              temp->right = new BSTNode();
              temp->right->data = data;
              dataPushed = true;
            }else{
              temp = temp->right;
            }
          }else{
            //data already present let it go
            temp = NULL;
          }
        }
        return 0;
      }
    }

    void printTree(){
      vector<BSTNode*> nodes;
      nodes.push_back(root);
      while(nodes.size() != 0){
        BSTNode* temp = nodes[nodes.size() - 1];
        cout << temp->data << endl;
        nodes.pop_back();
        if(temp->left != NULL)
          nodes.push_back(temp->left);
        if(temp->right != NULL)
          nodes.push_back(temp->right);
      }
    }

    int getDistance(int dataPoint1, int dataPoint2){
      //start from the root and look for the lowest common 
      BSTNode *temp = root;
      bool lcaFound = false;
      if(dataPoint1 > dataPoint2){
        int temp = dataPoint1;
        dataPoint1 = dataPoint2;
        dataPoint2 = temp;
      }
      while(!lcaFound){
        if(temp->data > dataPoint1 && temp->data < dataPoint2){
          //this is the lowest common ancestor 
          lcaFound = true;
        }else if(temp->data > dataPoint1 && temp->data > dataPoint2){
          temp = temp->left;
        }else if(temp->data < dataPoint1 && temp->data < dataPoint2){
          temp = temp->right;
        }else if(temp->data == dataPoint1){
          lcaFound = true;
        }else if(temp->data == dataPoint2){
          lcaFound = true;
        }
      }

      //from this node find the distance of node1 (Datapoint1) and node2 (Datapoint2);
      vector<BSTNode*> level1;
      vector<BSTNode*> level2;

      int distance1 = 0, distance2 = 0; bool data1Found = false;
      bool data2Found = false;
      level1.push_back(temp);
      while(!(data1Found && data2Found)){
        if(level1.size() > 0){
          while(level1.size() != 0){
            BSTNode* temp1 = level1[level1.size()-1];
            level1.pop_back();
            if(temp1->data == dataPoint1){
              data1Found = true;
            }
            if(temp1->data == dataPoint2){
              data2Found = true;
            }
            if(temp1->left != NULL)
              level2.push_back(temp1->left);
            if(temp1->right != NULL)
              level2.push_back(temp1->right);
          }
          if(!data1Found){
            distance1++;
          }
          if(!data2Found){
            distance2++;
          }
        }else if(level2.size() > 0){
          while(level2.size() != 0){
            BSTNode* temp1 = level2[level2.size()-1];
            level2.pop_back();
            if(temp1->data == dataPoint1){
              data1Found = true;
            }
            if(temp1->data == dataPoint2){
              data2Found = true;
            }
            if(temp1->left != NULL)
              level1.push_back(temp1->left);
            if(temp1->right != NULL)
              level1.push_back(temp1->right);
          }
          if(!data1Found){
            distance1++;
          }
          if(!data2Found){
            distance2++;
          }
        }
      }
      return distance1+distance2;
    }
};

int main(){
  BST bst;
  bst.addNode(5);
  bst.addNode(3);
  bst.addNode(4);
  bst.addNode(6);
  bst.addNode(7);
  bst.addNode(9);
  bst.addNode(8);
  bst.addNode(1);
  bst.addNode(2);
  bst.addNode(0);
  bst.printTree();
  cout << "The distance between datapoints 2 and 3 is : " << bst.getDistance(4, 0) << endl;
} 
