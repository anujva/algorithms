#include <iostream>
#include <cmath>

using namespace std;

int BinaryConverter(string str) { 

  // code goes here
  int number=0;
  for(int i=0; i < (int)str.size(); i++){
    if((str[i] - 48) == 1){
      number += pow(2,i);
    }
  }
  return number; 
            
}

int main() { 
  
  // keep this function call here
  string str = "100101";
  cout << BinaryConverter(str);
  return 0;
    
} 
