#include <iostream>
#include <cstring>
#include <sstream>
#include <vector>
#include <algorithm>
#include <cstdlib>

using namespace std;

class Event{
  public:
    int starttime;
    int endtime;

    Event(int hour1, int minute1, int hour2, int minute2){
      starttime = hour1*60+minute1;
      endtime = hour2*60+minute2;
    }
};

bool sortEvents(Event evt1, Event evt2){
  return evt1.starttime < evt2.starttime;
}

string MostFreeTime(vector<string> strArr){
  vector<Event> events;
  for(int i=0; i<(int)strArr.size(); i++){
    if(strArr[i][5] == 'P'){
      string hours1 = strArr[i].substr(0,2);
      string minutes1 = strArr[i].substr(3,2);
      int hour1, minute1;
      hour1 = atoi(hours1.c_str());
      minute1 = atoi(minutes1.c_str());
      if(hour1 > 1 && hour1 < 12){
        hour1 += 12;
      }
      string hours2 = strArr[i].substr(8,2);
      string minutes2 = strArr[i].substr(11,2);
      int hour2, minute2;
      hour2 = atoi(hours2.c_str());
      minute2 = atoi(minutes2.c_str());
      if(hour2 > 1 && hour2 < 12){
        hour2 += 12;
      }
      events.push_back(Event(hour1, minute1, hour2, minute2));
    }else if(strArr[i][5] == 'A' && strArr[i][13] == 'A'){
      string hours1 = strArr[i].substr(0,2);
      string minutes1 = strArr[i].substr(3,2);
      int hour1, minute1;
      hour1 = atoi(hours1.c_str());
      minute1 = atoi(minutes1.c_str());
      string hours2 = strArr[i].substr(8,2);
      string minutes2 = strArr[i].substr(11,2);
      int hour2, minute2;
      hour2 = atoi(hours2.c_str());
      minute2 = atoi(minutes2.c_str());
      events.push_back(Event(hour1, minute1, hour2, minute2));
    }else if(strArr[i][5] == 'A' && strArr[i][13] == 'P'){
      string hours1 = strArr[i].substr(0,2);
      string minutes1 = strArr[i].substr(3,2);
      int hour1, minute1;
      hour1 = atoi(hours1.c_str());
      minute1 = atoi(minutes1.c_str());
      string hours2 = strArr[i].substr(8,2);
      string minutes2 = strArr[i].substr(11,2);
      int hour2, minute2;
      hour2 = atoi(hours2.c_str());
      minute2 = atoi(minutes2.c_str());
      if(hour2 > 1 && hour2 < 12){
        hour2 += 12;
      }
      events.push_back(Event(hour1, minute1, hour2, minute2));
    }
  }
  sort(events.begin(), events.end(), sortEvents);
  vector<int> timediff;
  for(int i=0; i<(int)events.size()-1; i++){
    timediff.push_back(events[i+1].starttime - events[i].endtime);
  }
  sort(timediff.begin(), timediff.end());
  int hour = timediff[1]/60;
  int minute = timediff[1]%60;
  stringstream retVal;
  if(hour/10 == 0)
    retVal << "0" << hour;
  else
    retVal << hour;
  if(minute/10 == 0){
    retVal << ":0" << minute; 
  }else{
    retVal << ":" << minute;
  }
  return retVal.str();
}

int main(){
  string strArray[] = {"07:00AM-08:00AM","10:00PM-11:00PM","09:00AM-09:15PM"};
  vector<string> strArr(strArray, strArray+sizeof(strArray)/sizeof(strArray[0]));
  cout << MostFreeTime(strArr) << endl;
}
