#include <iostream>
#include <vector>

using namespace std;

int MeanMode(vector<int> arr){
  int countOfAppearance = 0; int maxAppearance = 0; int mode = 0;
  for(int i=0; i < (int)arr.size(); i++){
    for(int j=i+1; j < (int) arr.size(); j++){
      if(arr[i] == arr[j]){
        countOfAppearance++;
      }
    }
    if(countOfAppearance > maxAppearance){
      maxAppearance = countOfAppearance;
      mode = arr[i];
    }
  }

  float mean = 0;
  for(int i=0; i<arr.size(); i++){
    mean += arr[i];
  }
  mean = mean/(float)arr.size();

  if(mode == mean){
    return 1;
  }
  return 0;
}

int main(){
  int A[] = {1, 2, 3};
  vector<int> arr(A, A+sizeof(A)/sizeof(A[0]));
  cout << MeanMode(arr) << endl;
}
