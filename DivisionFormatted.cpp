#include <iostream>
#include <sstream>
#include <string>
#include <cmath>

using namespace std;

string DivisionStringified(int num1, int num2){
  double result = num1/(double)num2;
  float decimal = result - (int)result;
  decimal = decimal*10000;
  int decimalInt = (int)round(decimal);
  stringstream oss("");
  int num = (int)result;
  int count = 0;
  while(num != 0){
    int rem = num % 10;
    count++;
    num = num/10;
    if(count%3 == 0 && num != 0){
      string temp = oss.str();
      oss.str("");
      oss << "," << rem << temp;
    }else{
      string temp = oss.str();
      oss.str("");
      oss << rem << temp;
    }
  }
  if((int)result == 0){
    oss << 0;
  }
  string temp = oss.str();
  oss.str("");
  if(decimalInt != 0)
    oss << temp << "." << decimalInt;
  else
    oss << temp << ".0000";
  return oss.str();
}

int main(){
  cout << DivisionStringified(123456789, 10000) << endl;
}
