#include <iostream>
#include <sstream>

using namespace std;

string DashInsertII(int num) { 

  // code goes here
  stringstream oss;
  oss << num;
  string number = oss.str();
  
  for(int i=0; i < (int)number.size()-1; i++){
    int tempnum1 = number[i]-48;
    int tempnum2 = number[i+1]-48;
    if((tempnum1 % 2 != 0) && (tempnum2 % 2 != 0)){
      //two consecutive odd numbers
      string tempstring1 = number.substr(0, i+1);
      string tempstring2 = number.substr(i+1, number.size() - i - 1);
      number = tempstring1 + "-" + tempstring2;
      i++;
    }
    if((tempnum1 % 2 == 0) && (tempnum2 % 2 == 0)){
      //two consecutive even numbers
      if(tempnum1 == 0 || tempnum2 == 0){
        continue;
      }
      if(tempnum2 == 0){
        continue;
      }
      string tempstring1 = number.substr(0, i+1);
      string tempstring2 = number.substr(i+1, number.size() - i - 1);
      number = tempstring1 + "*" + tempstring2;
      i++;
    }
  }
  
  return number; 
}

int main() { 
  
  // keep this function call here
  
  cout << DashInsertII(56647304);
  return 0;
    
} 
