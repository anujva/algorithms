#include <iostream>
#include <vector>

using namespace std;

int main(){
  int a[] = {-1, 0, 1, 2, -1, 4};
  vector<int> arr(a, a+sizeof(a)/sizeof(a[0]));

  for(int i=0; i < (int)arr.size(); i++){
    for(int j=i+1; j < (int)arr.size(); j++){
      for(int k=j+1; k < (int)arr.size(); k++){
        if(arr[i]+arr[j]+arr[k] == 0){
          cout << i << " " << j << " " << k << endl;
        }
      }
    }
  }
}
