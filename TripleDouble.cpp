#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

int TripleDouble(int num1, int num2){
  vector<char> allTriples;
  vector<char> allDoubles;
  stringstream oss, oss1;
  oss << num1;
  string number1;
  oss >> number1;
  oss1 << num2;
  string number2;
  oss1 >> number2;

  cout << number1 << " " << number2 << endl;

  for(int i = 0; i < (int)number1.size() - 2; i++){
    int count = 0;
    for(int j = i+1; j < i+3; j++){
      if(number1[i] == number1[j]){
        count++;
      }
    }
    if(count > 1){
      allTriples.push_back(number1[i]);
    }
  }
  cout << allTriples[0] << endl;

  for(int i = 0; i < (int) number2.size()-1; i++){
    int count = 0;
    if(number2[i] == number2[i+1]){
      count++;
    }
    if(count > 0){
      allDoubles.push_back(number2[i]);
    }
  }

  for(int i=0; i < (int)allTriples.size(); i++){
    for(int j=0; j < (int) allDoubles.size(); j++){
      if(allTriples[i] == allDoubles[j]){
        return 1;
      }
    }
  }
  return 0;
}

int main(){
  cout << TripleDouble(556668, 556886) << endl;
  return 0;
}
