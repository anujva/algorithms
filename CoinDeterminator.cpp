#include <iostream>
#include <climits>
using namespace std;

int minimum(int a, int b, int c, int d, int e){
  int temp = a < b ? a : b;
  temp = temp < c ? temp : c;
  temp = temp < d ? temp : d;
  temp = temp < e ? temp : e;

  return temp;
}

int CoinDeterminator(int num){
  int minVal[num+1];
  for(int i=0; i<=num; i++){
    minVal[i] = 0;
  }

  for(int i=1; i<=num; i++){
    int temp1 = INT_MAX, temp2 = INT_MAX, temp3 = INT_MAX, temp4 = INT_MAX, temp5 = INT_MAX;
    if(i-1 > -1){
      temp1 = minVal[i-1]+1;
    }
    if(i-5 > -1){
      temp2 = minVal[i-5]+1;
    }
    if(i-7 > -1){
      temp3 = minVal[i-7]+1;
    }
    if(i-9 > -1){
      temp4 = minVal[i-9]+1;
    }
    if(i-11 > -1){
      temp5 = minVal[i-11]+1;
    }
    int temp = minimum(temp1, temp2, temp3, temp4, temp5);
    minVal[i] = temp;
  }

  return minVal[num];
}

int main(int argc, char *argv[]){
  cout << CoinDeterminator(5) << endl;
  cout << CoinDeterminator(7) << endl;
  cout << CoinDeterminator(9) << endl;
  cout << CoinDeterminator(10) << endl;
  cout << CoinDeterminator(20) << endl;
  cout << CoinDeterminator(31) << endl;
  cout << CoinDeterminator(35) << endl;
}
