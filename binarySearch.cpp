#include <iostream>
#include <vector>
#include <string>

using namespace std;

int BinarySearch(vector<int> array, int searchElement, int index1, int index2){
  if(index1 > index2){
    return -1;
  }else{
    int mid = index1+index2;
    mid = mid/2;
    if(array[mid] == searchElement){
      return mid;
    }else if(array[mid] < searchElement){
      index1 = mid+1;
      return BinarySearch(array, searchElement, index1, index2);
    }else if(array[mid] > searchElement){
      index2 = mid-1;
      return BinarySearch(array, searchElement, index1, index2);
    }
  }
}

int main(){
  int arr[] = {1, 4, 6, 8, 9};
  vector<int> array(arr, arr+sizeof(arr)/sizeof(arr[0]));
  int ret = BinarySearch(array, 4, 0, array.size()-1);
  cout << "Element found at: " << ret << endl;
}
