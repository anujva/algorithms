#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <sstream>

using namespace std;

string TransitivityRelations(vector<string> strArr){
  vector< vector<int> > matrix;
  matrix.resize(strArr.size());
  for(int i=0; i<(int)strArr.size(); i++){
    matrix[i].resize(strArr.size());
  }

  //create a matrix from the string now.. will be easier..
  for(int i=0; i < (int)strArr.size(); i++){
    string str = strArr[i];
    int count = 0;
    for(int j=1; j < (int)str.size(); j=j+2){
      matrix[i][count++] = str[j]-48;
    }
  }

  vector< pair<int, int> > transitive;
  for(int i=0; i < (int)matrix.size(); i++){
    for(int j=0; j < (int)matrix.size(); j++){
      if(matrix[i][j] == 1 && i != j){
        for(int k=0; k < (int)matrix.size(); k++){
          if(matrix[j][k] == 1 && j != k){
            if(i != k && matrix[i][k] == 1){
              //transitivity holds.. keep going.
            }else{
              if(i == k){
                continue;
              }
              transitive.push_back(make_pair(i, k));
              matrix[i][k] = 1;
              j = 0;
            }
          }
        }
      }
    }
  }

  if(transitive.size() == 0){
    return string("transitive");
  }

  stringstream oss;
  for(int i=0; i < (int)transitive.size(); i++){
    oss << "(" << transitive[i].first << "," << transitive[i].second << ")";
    if(i != (int)transitive.size() - 1){
      oss << "-";
    }
  }
  return oss.str();
}

int main(){
  string A[] = {"(1,1,0,0)","(0,0,1,0)","(0,1,0,1)","(1,0,0,1)"};

  vector<string> strArr(A, A+sizeof(A)/sizeof(A[0]));
  cout << TransitivityRelations(strArr);
}
