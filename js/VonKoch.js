window.onload = draw;

var VanKoch = function(x, y, sideLength){
  this.startx = x;
  this.starty = y;
  this.sideLength = sideLength;
  this.angle = 0.0;
}

VanKoch.prototype.left = function(angleChange){
  this.angle -= angleChange;
}

VanKoch.prototype.right = function(angleChange){
  this.angle += angleChange;
}

VanKoch.prototype.drawFourLines = function(sideLength, level){
  if(level == 0){
    ctx.moveTo(this.startx, this.starty);
    var x_new = this.startx + sideLength*Math.cos(this.angle*Math.PI/180);
    var y_new = this.starty + sideLength*Math.sin(this.angle*Math.PI/180);
    ctx.lineTo(x_new, y_new);
    this.startx = x_new;
    this.starty = y_new;
  }else{
    this.drawFourLines(sideLength/3, level-1);
    this.left(60);
    this.drawFourLines(sideLength/3, level-1);
    this.right(120);
    this.drawFourLines(sideLength/3, level-1);
    this.left(60);
    this.drawFourLines(sideLength/3, level-1);
  }
}

VanKoch.prototype.snowflake(){
  for(var i=0; i<3; i++){
    this.drawFourLines(this.sideLength, 3);
    this.right(120);
  }
}

function draw(){
  var canvas = document.getElementById('canvas1');
  canvas.style.border = "thick solid #000";
  canvas.width = 600;
  canvas.height = 480;
  if(canvas && canvas.getContext){
    ctx = canvas.getContext("2d");
    if(ctx){
      //setup some drawing information
      ctx.strokeStyle = "red";
      ctx.fillStyle = "yellow";
      ctx.lineWidth = 1;

      ctx.beginPath();
      var vk = new VanKoch(120, 120, 300);
      vk.snowflake(); 
      ctx.stroke();
    }
  }
}

