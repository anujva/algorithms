#include <iostream>
#include <vector>

using namespace std;

bool checkPrime(int num){
  if(num == 2 || num == 3 || num == 5 || num == 7 || num == 11){
    return true;
  }

  if((num % 2 == 0) || (num % 3 == 0) || (num % 5 == 0) || (num % 7) == 0){
    return false;
  }

  for(int i=11; i < num; i = i+2){
    if(num % i == 0){
      return false;
    }
  }

  return true;
}

int PrimeMover(int num){
  vector<int> primes;
  int number = 2;
  while(primes.size() != (unsigned long)num){
    //check if this number is prime if its put onto primes vector;
    if(checkPrime(number)){
      primes.push_back(number);
    }
    number++;
  }
  return primes[primes.size() - 1]; 
}

int main(){
  cout << PrimeMover(312) << endl;
}
