#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>

using namespace std;

string ArrayAdditionI(vector<int> arr){
  sort(arr.begin(), arr.end());
  while(arr[0] < 0){
    arr[arr.size()-1] += arr[0];
    arr.erase(arr.begin());
    sort(arr.begin(), arr.end());
  }
  int MaxSum = arr[arr.size()-1];
  int M[MaxSum+1][arr.size()-1];
  for(int i=0; i<MaxSum+1; i++){
    for(int j=0; j<arr.size()-1; j++){
      M[i][j] = 0;
    }
  }
  for(int i=0; i<MaxSum+1; i++){
    if(arr[0] <= i)
      M[i][0] = 1;
  }
  
  for(int j=0; j<arr.size()-1; j++){
    M[0][j] = 0;
  }

  for(int i=1; i<MaxSum+1; i++){
    //M[0] = 0, M[1] = (either 1 is there in the array or M
    //check for all the numbers in the array less than i
    for(int j=1; j<arr.size()-1; j++){
      if(i-arr[j] > -1){
        M[i][j] = max(M[i][j-1], M[i-arr[j]][j-1]+1);
      }else
        M[i][j] = M[i][j-1];
    }
  }
  for(int i=0; i<MaxSum+1; i++){
    for(int j=0; j<arr.size()-1; j++){
      cout << M[i][j] << " ";
    }
    cout << endl;
  }
  //Now this array should be filled 
  //We have to look up the array in reverse to backtrace the path to see which elements were included.
  vector<int> elements_included;
  int ii = MaxSum; int jj = arr.size() - 2;
  while(jj > 0){
    //check to see if M[i][j] = M[i][j-1] or M[i-arr[j]] + 1
    if(M[ii][jj] == (M[ii-arr[jj]][jj-1] + 1)){
      elements_included.push_back(jj);
      ii = ii - arr[jj];
      jj = jj-1;
    }else if(M[ii][jj] == M[ii][jj-1]){
      jj = jj-1;
    }
  }
  //Now just test for the first value
  //whether it should be included in nor not
  if(M[ii][jj] == 1){
    elements_included.push_back(jj);
  }

  //find the sum of the included elements
  //see if they are equal to the MaxSum
  //if it is true then return true;
  int checkSum = 0;
  for(int k=0; k<elements_included.size(); k++){
    checkSum += arr[elements_included[k]];
  }
  if(checkSum == MaxSum){
    return "true";
  }
  return "false";
}

int main(int argc, char* argv[]){
  int arr[] = {54,49,1,0,7,4};//{3,5,-1,8,12};
  vector<int> array(arr, arr+sizeof(arr)/sizeof(arr[0]));
  cout << ArrayAdditionI(array) << endl;
}
