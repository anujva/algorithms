#include <iostream>
#include <cmath>
#include <vector>
#include <sstream>
#include <cstdlib>

using namespace std;

int MatrixDeterminantCalc(vector< vector<int> > matrix){
  if(matrix.size() == 2){
    return matrix[0][0]*matrix[1][1] - matrix[0][1]*matrix[1][0];
  }else{
    int res = 0;
    for(int i=0; i < (int)matrix.size(); i++){
      vector< vector<int> > matrix_seed;
      matrix_seed.resize(matrix.size() - 1);
      for(int j=0; j < (int)matrix.size() - 1; j++){
        matrix_seed[j].resize(matrix.size() - 1);
      }

      for(int j = 0; j < (int)matrix_seed.size(); j++){
        int matrix_col_index = 0;
        for(int k = 0; k < (int)matrix_seed.size(); k++){
          if(k == i){
            matrix_col_index++;
          }
          matrix_seed[j][k] = matrix[j+1][matrix_col_index++];
        }
      }

      res += matrix[0][i]*MatrixDeterminantCalc(matrix_seed)*pow(-1, i);
    }
    return res;
  }
}

bool testMatrix(vector<string> strArr){
  int columnLength = 0;
  for(int i=0; i < (int)strArr.size(); i++){
    if(strArr[i][0] == '<'){
      break;
    }else{
      columnLength++;
    }
  }

  cout << columnLength << endl;

  for(int i=columnLength; i < (int)strArr.size(); i=i+columnLength+1){
    if(strArr[i][0] != '<'){
      return false;
    }
  }

  return true;
}

int MatrixDeterminant(vector<string> strArr) { 
  if(!testMatrix(strArr)){
    return -1;
  }
  int rowcount = 0; int columncount = 0;
  vector< vector<int> > matrix;
  for(int i=0; i < (int)strArr.size(); i++){
    if(strArr[i][0] == '<'){
      //do nothing
      break;
    }else{
      columncount++;
    }
  }

  matrix.resize(columncount);
  for(int i=0; i < columncount; i++){
    matrix[i].resize(columncount);
  }
  
  columncount = 0;
  for(int i=0; i < (int)strArr.size(); i++){
    if(strArr[i][0] == '<'){
      rowcount++;
      columncount = 0;
    }else{
      matrix[rowcount][columncount++] = atoi(strArr[i].c_str());
    }
  }

  //we have the matrix now.. now to perform the determinant calculation
  return MatrixDeterminantCalc(matrix); 
}

int main() { 
   
  // keep this function call here
  /* Note: In C++ you first have to initialize an array and set 
     it equal to the stdin to test your code with arrays. 
     To see how to enter arrays as arguments in C++ scroll down */
     
  string A[] = {"1","2","4","<>","2","1","1","<>","4","1","1"};
  vector<string> strArr(A, A+sizeof(A)/sizeof(A[0]));
  cout << MatrixDeterminant(strArr);
  return 0;
} 

