#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

class KeyedRadixSort{
  private:
    vector<char> arraycopy;
    vector<int> keyCount;
  public:
    KeyedRadixSort(vector<char> array){
      keyCount.resize(26+1);
      arraycopy.resize(array.size());
    }

    void sort(vector<char> &array){
      for(int i=0; i < 27; i++){
        keyCount[i] = 0;
      }

      for(int i=0; i < (int)array.size(); i++){
        keyCount[array[i]-97+1]++;
      }

      for(int i=2; i < 27; i++){
        keyCount[i] = keyCount[i-1] + keyCount[i];
      }

      for(int i=0; i < (int)array.size(); i++){
        arraycopy[keyCount[array[i]-97]++] = array[i];
      }

      for(int i=0; i < (int)array.size(); i++){
        array[i] = arraycopy[i];
      }
    }
};

class LSDRadixSort : public KeyedRadixSort{
  private:
    vector<int> stringLengths;
    vector< vector<char> > strChar;

  public:
    LSDRadixSort(vector<string> strArr){
      for(int i=0; i < (int)strArr.size(); i++){
        stringLengths.push_back(strArr[i].size());

      }
    }
};

int main(){
  char arr[] = {'b', 'v', 'n', 'm', 'd', 'w', 'e', 'r', 't', 'q'};
  vector<char> array(arr, arr+sizeof(arr)/sizeof(arr[0]));
  KeyedRadixSort k(array);
  k.sort(array);
  for(int i=0; i < (int)array.size(); i++){
    cout << array[i] << " ";
  }
  cout << endl;
}
