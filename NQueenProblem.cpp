#include <iostream>
#include <vector>

using namespace std;

void makeAvailable(bool **&availableBoardSquare, int size, int row, int column){
  for(int i=0; i<size; i++){
    availableBoardSquare[row][i] = true;
    availableBoardSquare[i][column] = true;
  }
}

void makeUnavailable(bool **&availableBoardSquare, int size, int row, int column){
  for(int i=0; i<size; i++){
    availableBoardSquare[row][i] = false;
    availableBoardSquare[i][column] = false;
  }
}

void putQueen(int row, int size, bool **&availableBoardSquare){
  for(int i=0; i<size; i++){
    if(availableBoardSquare[row][i] == true){
      makeUnavailable(availableBoardSquare, size, row, i);
      putQueen(row+1, size, availableBoardSquare);
      makeAvailable(availableBoardSquare, size, row, i);
    }
  }
}

int main(){
  bool **availableBoardSquare;
  cout << "Please enter the size of the board: ";
  int size;
  cin >> size;
  availableBoardSquare = new bool*[size];
  for(int i=0; i<size; i++){
    availableBoardSquare[i] = new bool[size];
  }

  for(int i=0; i<size; i++){
    for(int j=0; j<size; j++){
      availableBoardSquare[i][j] = true;
    }
  }

  putQueen(0, size, availableBoardSquare);
  return 0;
}
