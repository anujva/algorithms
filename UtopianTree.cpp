#include <iostream>
#include <string>

using namespace std;

int main(){
  cout << "Enter the value for T: ";
  int T;
  cin >> T;
  int *N;
  N = new int[T];
  for(int i=0; i<T; i++){
    cout << "Enter the Value of N for " << i << " test case number: ";
    cin >> N[i];
  }

  int *height = new int[T];
  for(int i=0; i<T; i++){
    height[i] = 1;
  }

  for(int i=0; i<T; i++){
    for(int j=0; j<N[i]; j++){
      if(j%2 == 0){
        height[i] *= 2;
      }else{
        height[i] += 1;
      }
    }
  }
  
  cout << endl << endl;
  for(int i=0; i<T; i++){
    cout << height[i] << endl;
  }
}
