#include <iostream>
#include <vector>
#include <cstring>
#include <algorithm>

using namespace std;

void split(string str, char delim, vector<string> &elems){
  int stringsize = str.size();
  int count = 0;
  int i = 0;
  while(count++ != stringsize){
    if(str[i++] == delim){
      elems.push_back(str.substr(0, i-1));
      str.erase(0, i);
      i = 0;
    }
  }
  elems.push_back(str);
}

string Palindrome(string str) { 

  // code goes here   
  transform(str.begin(), str.end(), str.begin(), ::tolower);
  for(int i=0; i < (int)str.size(); i++){
    if(!(str[i] > 96 && str[i] < 123)){
      str[i] = ' ';
    }
  }
  char delimiter = ' ';
  vector<string> elems;
  split(str, delimiter, elems);
  string result = string("");
  for(int i=0; i < (int)elems.size(); i++){
    result += elems[i];
  }

  for(int i=0; i < (int)result.size()/2; i++){
    if(result[i] != result[result.size()-1-i]){
      return "false";
    }
  }
  return "true"; 
}

int main() { 
  // keep this function call here
  cout << Palindrome("A war at Tarawa") << endl;
  return 0;
}
