#include <iostream>
#include <sstream>
#include <string>
#include <cmath>

using namespace std;

string DivisionStringified(int num1, int num2){
  float result = (float)num1/(float)num2;
  result = round(result);
  stringstream oss("");
  int num = result;
  int count = 0;
  while(num != 0){
    int rem = num % 10;
    count++;
    num = num/10;
    if(count%3 == 0 && num != 0){
      string temp = oss.str();
      oss.str("");
      oss << "," << rem << temp;
    }else{
      string temp = oss.str();
      oss.str("");
      oss << rem << temp;
    }
  }
  return oss.str();
}

int main(){
  cout << DivisionStringified(5, 2) << endl;
}
